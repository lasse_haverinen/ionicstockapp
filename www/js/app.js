// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic', 'ngCordova']);

app.config(['$ionicConfigProvider', '$stateProvider','$urlRouterProvider', function($ionicConfigProvider, $stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('main', {
        url: '/',
        templateUrl: "main.html",
        controller: "mainCtrl"
    })
    .state('detail', {
        url: '/detail/:symbol/:following?',
        templateUrl: "detail.html",
        controller: "detailCtrl"
    })
    .state('search', {
        url: '/search',
        templateUrl: "search.html",
        controller: "searchCtrl"
    });
    

  
}]);

app.controller('mainCtrl', ['$scope', 'CurrentDataService', '$ionicPlatform',  function($scope, CurrentDataService, $ionicPlatform){
    $scope.stockData = [];
    $ionicPlatform.ready(function() {
        
        $scope.stockData = CurrentDataService.getFullData().then(function (data){
            $scope.stockData = data;    
            console.log("PROMISE HOIDETTU");
        });

    });
    
}]);

app.controller('detailCtrl', ['$scope', 'CurrentDataService', '$stateParams', '$state', '$ionicHistory', '$cordovaVibration', '$ionicPlatform', function($scope, CurrentDataService, $stateParams, $state, $ionicHistory, $cordovaVibration, $ionicPlatform){    
    $scope.data = {};
    $scope.following = $stateParams.following;
    
    
    $scope.data = CurrentDataService.getStockData($stateParams.symbol, $scope.following);
    
    $scope.follow = function()
    {        
        CurrentDataService.followPreview();
        
        // disable back functionality for next view, we are going back to main menu
        $ionicHistory.nextViewOptions({
            disableBack: true
          });
        // vibrate when adding
        $ionicPlatform.ready(function() {
            $cordovaVibration.vibrate(100);
        });
        $state.go('main');
    }    
}]);

app.controller('searchCtrl', ['$scope', 'CurrentDataService', '$state',function($scope, CurrentDataService, $state){    
    $scope.searchData = { searchText: "", results: []};
    $scope.loading = false;
    
    $scope.search = function()
    {
        if($scope.searchData.searchText == "")
        {
            return;
        }
        
        $scope.loading = true;
        CurrentDataService.searchStock($scope.searchData.searchText).then(function(data){
            $scope.loading = false;
            
            $scope.searchData.results = [];
            $scope.searchData.results.push(data);
            
        });        
    }
    
    $scope.details = function(data)
    {
        $scope.searchData = { searchText: "", results: []};
        $state.go('detail', { symbol : data.symbol });
    }
}]);

app.run(['$ionicPlatform', '$cordovaSQLite', function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    /*if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }*/


    db = $cordovaSQLite.openDB({ name: "stockaApp.db" });
    var query = "CREATE TABLE IF NOT EXISTS stockdata (symbol TEXT NULL, data TEXT NULL)";

    $cordovaSQLite.execute(db, query).then(function(res) {
        console.log("CREATE TABLE IF NOT EXISTS SUCCESS");
    }, function (err) {
        console.error(err);
    });
    
  });
}])

app.factory('CurrentDataService', ['$http', '$cordovaSQLite', '$ionicPlatform', '$q', function($http, $cordovaSQLite, $ionicPlatform, $q) {
    var data = [];//[{symbol: "aapl", change: "+0.5%"}, {symbol: "fslr", change: "-0.44%"}];
    var previewData;
    
    return {
        getFullData: function()
        {
            var d = $q.defer();

            var query = "SELECT data from stockdata";

            $cordovaSQLite.execute(db, query).then(function(res) {
                console.log("select length: " + res.rows.length);

                data = [];
                for(var i = 0; i < res.rows.length; i++)
                {
                    console.log(res.rows.item(i).data);
                    data.push(JSON.parse(res.rows.item(i).data));
                }

                d.resolve(data);

            }, function (err) {
                console.error(err);
            });
        
            return d.promise;
        },           

        getPreviewData: function()
        {
            return previewData;
        },
        
        getStockData: function(symbol, following)
        {
            if(previewData != null && following == false && previewData.symbol == symbol)
            {
                return previewData;
            }
            else
            {
                for(var i = 0; i < data.length; i++)
                {
                    if(data[i].symbol == symbol)
                    {
                        return data[i];
                    }
                }
            }
            
            return null;
        },
        
        followPreview: function()            
        {
            // Check if exists
            for(var i = 0; i < data.length; i++)
            {
                if(data[i].symbol == previewData.symbol)
                {
                    return;
                }
            }

            var query = "INSERT INTO stockdata (symbol, data) VALUES (?, ?)";
            $cordovaSQLite.execute(db, query, [previewData.symbol, JSON.stringify(previewData)]).then(function(res) {
                console.log("Follow stock " + previewData.symbol + " inserted");
            }, function (err) {
                console.error(err);
            });            

            data.push(previewData);            
        },
        
        searchStock: function(symbol)
        {
            var d = $q.defer();
            $http.get('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22' + symbol + '%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=').
                success(function(data, status, headers, config) {
                                     
                    previewData = data.query.results.quote;
                    var change = data.query.results.quote.Change;                    
                    if(change != null)
                    {
                        if(change[0] == "+")
                        {
                            previewData.rising = true;											
                        }
                        else
                        {
                            previewData.rising = false;
                        }   
                    }
                       
                    d.resolve(previewData);
                });

            return d.promise;
        }
    }
}]);
